Issue explanation
====
list with links
1. [Milestone 1](#mile-01)
  1. [The first step](#mile-01-step1)
  2. [The second step](#mile-01-step2)
  3. [The third step](#mile-01-step3)

## <a name="mile-01">The project's part 1</a>
description
### <a name="mile-01-step1">The very first step</a>
description
### <a name="mile-01-step2">Another step</a>
description
### <a name="mile-01-step3">Yet another step, maybe the last</a>
description
